#!/usr/bin/env bash

# this can be used to extract the data schema of an
# existing database to a Liquibase YAML changelog file
# see liquibase.properites in the same folder

export LIQUIBASE_HOME="~/Programs/Liquibase-3-5-5"
export PATH=$LIQUIBASE_HOME:$PATH
liquibase  generateChangeLog
# --diffTypes="data"
