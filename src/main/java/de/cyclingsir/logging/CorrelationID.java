package de.cyclingsir.logging;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.UUID;

/**
 * Created by M.T. on 12.01.17.
 */
@EqualsAndHashCode(of = "id")
@Getter
public final class CorrelationID {
    public static final String KEY = "correlationID";
    private final UUID id;

    private CorrelationID() {
        this.id = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return id.toString();
    }

    public static CorrelationID createNewID() {
        return new CorrelationID();
    }

    public String getLeftCharacters(int count) {
        return this.id.toString().substring(0, count - 1);
    }
}
