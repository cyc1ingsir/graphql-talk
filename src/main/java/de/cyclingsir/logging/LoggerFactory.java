package de.cyclingsir.logging;


import org.apache.logging.log4j.LogManager;

/**
 * Created by M.T. on 11.01.17.
 */
public final class LoggerFactory {

    private LoggerFactory() {

    }

    public static Logger getLogger(Class<?> clazz) {
        return new Logger(LogManager.getLogger(clazz));
    }
}
