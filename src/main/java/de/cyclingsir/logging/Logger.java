package de.cyclingsir.logging;

import lombok.Getter;
import lombok.NonNull;

/**
 * Created by M.T. on 11.01.17.
 */
@Getter
public class Logger {
    private final org.apache.logging.log4j.Logger log4jLogger;

    protected Logger(org.apache.logging.log4j.Logger logger) {
        this.log4jLogger = logger;
    }

    public void debug(@NonNull String message) {
        log4jLogger.debug(message);
    }

    public void debug(@NonNull String message, @NonNull Object... params) {
        log4jLogger.debug(message, params);
    }


    public void debug(@NonNull String message, @NonNull Throwable t) {
        log4jLogger.debug(message, t);
    }

    public void error(@NonNull Throwable t) {
        log4jLogger.error(t);
    }

    public void error(@NonNull String message) {
        log4jLogger.error(message);
    }

    public void error(@NonNull String message, @NonNull Object... params) {
        log4jLogger.error(message, params);
    }

    public void error(@NonNull String message, @NonNull Throwable t) {
        log4jLogger.error(message, t);
    }

    public void fatal(@NonNull String message) {
        log4jLogger.fatal(message);
    }

    public void fatal(@NonNull String message, @NonNull Object... params) {
        log4jLogger.fatal(message, params);
    }

    public void fatal(@NonNull String message, @NonNull Throwable t) {
        log4jLogger.fatal(message, t);
    }

    public void info(@NonNull String message) {
        log4jLogger.info(message);
    }

    public void info(@NonNull String message, @NonNull Object... params) {
        log4jLogger.info(message, params);
    }

    public void info(@NonNull String message, @NonNull Throwable t) {
        log4jLogger.info(message, t);
    }

    public void trace(@NonNull String message) {
        log4jLogger.trace(message);
    }

    public void trace(@NonNull String message, @NonNull Object... params) {
        log4jLogger.trace(message, params);
    }

    public void trace(@NonNull String additionalMessage, @NonNull Throwable t) {
        log4jLogger.trace(additionalMessage, t);
    }

    public void warn(@NonNull String message) {
        log4jLogger.warn(message);
    }

    public void warn(@NonNull String message, @NonNull Object... params) {
        log4jLogger.warn(message, params);
    }

    public void warn(@NonNull String message, @NonNull Throwable t) {
        log4jLogger.warn(message, t);
    }
}
