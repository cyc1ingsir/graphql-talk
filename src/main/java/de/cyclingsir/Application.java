package de.cyclingsir;

import de.cyclingsir.library.information.MyBanner;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Main-Klasse fuer die Spring-Boot-App
 */
// CHECKSTYLE:OFF
@SpringBootApplication
public class Application extends SpringBootServletInitializer {
// CHECKSTYLE:ON

   /**
     * @param args
     */
    public static void main(final String[] args) {
        new SpringApplicationBuilder()
            .banner(new MyBanner())
            .bannerMode(Banner.Mode.LOG)
            .sources(Application.class)
            .run(args);
    }

}
