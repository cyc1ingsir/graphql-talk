package de.cyclingsir.library.configuration;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ResourceLoader;

import javax.sql.DataSource;

/**
 * Created by cyc1ingsir on 25/03/18.
 */
@Configuration
@Profile("integration")
public class IntegrationConfiguration {

    @Bean(name = "liquibase")
    public SpringLiquibase liquibase(@Autowired DataSource dataSource, ResourceLoader resourceLoader) {
        SpringLiquibase liquibase = new SpringLiquibase();
//        base.setDropFirst(true);
        liquibase.setDataSource(dataSource);
        liquibase.setBeanName("liquibase");
        liquibase.setResourceLoader(resourceLoader);
        liquibase.setChangeLog("classpath:db/integration/db.integration-master.yaml");
        return liquibase;
    }
}
