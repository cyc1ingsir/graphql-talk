package de.cyclingsir.library.configuration;

import com.coxautodev.graphql.tools.GraphQLResolver;
import de.cyclingsir.library.entity.Author;
import de.cyclingsir.library.entity.Book;
import de.cyclingsir.library.entity.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by cyc1ingsir on 4/14/18.
 */
@Component
@RequiredArgsConstructor
public class AuthorResolver implements GraphQLResolver<Author> {

    private final BookRepository bookRepository;

    public List<Book> books(Author author) {
        return bookRepository.findByAuthorId(author.getId());
    }
}
