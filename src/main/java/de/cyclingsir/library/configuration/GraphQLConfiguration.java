package de.cyclingsir.library.configuration;

import com.coxautodev.graphql.tools.SchemaParserDictionary;
import de.cyclingsir.library.entity.Reader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by cyc1ingsir on 4/13/18.
 */
@Configuration
public class GraphQLConfiguration {

    @Bean
    public SchemaParserDictionary schemaParserDictionary() {
        return new SchemaParserDictionary()
            .add(Reader.class);
    }
}
