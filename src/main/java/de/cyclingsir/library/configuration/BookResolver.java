package de.cyclingsir.library.configuration;

import com.coxautodev.graphql.tools.GraphQLResolver;
import de.cyclingsir.library.entity.Book;
import de.cyclingsir.library.entity.BookCategory;
import de.cyclingsir.library.entity.BookCategoryRepository;
import de.cyclingsir.library.entity.Publisher;
import de.cyclingsir.library.entity.PublisherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by cyc1ingsir on 4/14/18.
 */
@Component
@RequiredArgsConstructor
public class BookResolver implements GraphQLResolver<Book> {

    private final PublisherRepository publisherRepository;

    private final BookCategoryRepository bookCategoryRepository;


    public Optional<Publisher> publisher(Book book) {
        return publisherRepository.findById(book.getPublisher().getId());
    }

    public Optional<BookCategory> category(Book book) {
        return bookCategoryRepository.findById(book.getCategory().getId());
    }
}
