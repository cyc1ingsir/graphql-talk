package de.cyclingsir.library.information;

import de.cyclingsir.logging.Logger;
import de.cyclingsir.logging.LoggerFactory;
import lombok.Getter;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringBootVersion;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.boot.ansi.AnsiOutput;
import org.springframework.boot.ansi.AnsiPropertySource;
import org.springframework.boot.ansi.AnsiStyle;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertyResolver;
import org.springframework.core.env.PropertySourcesPropertyResolver;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

/**
 * Prints a banner including various information such as
 * git commit ID, build version and time.
 * This is useful for determine the exact version of the
 * artifact currently running
 * Created by M.R. on 02/02/17.
 */
public class MyBanner implements Banner {

    private static final String[] BANNER = {"",
        "[38;5;118m [0m[38;5;118m [0m[38;5;118m [0m[38;5;118m [0m[38;5;118m [0m[38;5;118m [0m[38;5;118m [0m[38;5;118m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;148m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;178m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;209m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;204m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;199m [0m",
        "[38;5;118m [0m[38;5;118m [0m[38;5;118m [0m[38;5;118m [0m[38;5;118m [0m[38;5;154m_[0m[38;5;154m/[0m[38;5;154m_[0m[38;5;154m/[0m[38;5;154m_[0m[38;5;154m/[0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;148m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;178m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m_[0m[38;5;208m/[0m[38;5;208m [0m[38;5;209m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m_[0m[38;5;203m/[0m[38;5;203m_[0m[38;5;204m/[0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m_[0m[38;5;198m/[0m[38;5;198m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m",
        "[38;5;118m [0m[38;5;118m [0m[38;5;154m_[0m[38;5;154m/[0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;148m_[0m[38;5;184m/[0m[38;5;184m [0m[38;5;184m [0m[38;5;184m_[0m[38;5;184m/[0m[38;5;184m_[0m[38;5;184m/[0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;178m_[0m[38;5;214m/[0m[38;5;214m_[0m[38;5;214m/[0m[38;5;214m_[0m[38;5;214m/[0m[38;5;214m [0m[38;5;214m [0m[38;5;214m_[0m[38;5;214m/[0m[38;5;208m_[0m[38;5;208m/[0m[38;5;208m_[0m[38;5;208m/[0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m_[0m[38;5;209m/[0m[38;5;203m_[0m[38;5;203m/[0m[38;5;203m_[0m[38;5;203m/[0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m_[0m[38;5;203m/[0m[38;5;203m [0m[38;5;204m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m_[0m[38;5;198m/[0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m_[0m[38;5;199m/[0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m",
        "[38;5;154m [0m[38;5;154m_[0m[38;5;154m/[0m[38;5;154m [0m[38;5;154m [0m[38;5;154m_[0m[38;5;154m/[0m[38;5;154m_[0m[38;5;154m/[0m[38;5;148m [0m[38;5;184m [0m[38;5;184m_[0m[38;5;184m/[0m[38;5;184m_[0m[38;5;184m/[0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;178m_[0m[38;5;214m/[0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m_[0m[38;5;214m/[0m[38;5;214m [0m[38;5;214m [0m[38;5;208m_[0m[38;5;208m/[0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m_[0m[38;5;208m/[0m[38;5;208m [0m[38;5;209m [0m[38;5;203m_[0m[38;5;203m/[0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m_[0m[38;5;203m/[0m[38;5;203m [0m[38;5;203m [0m[38;5;203m_[0m[38;5;204m/[0m[38;5;198m [0m[38;5;198m [0m[38;5;198m_[0m[38;5;198m/[0m[38;5;198m_[0m[38;5;198m/[0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;199m [0m[38;5;199m_[0m[38;5;199m/[0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;163m [0m",
        "[38;5;154m_[0m[38;5;154m/[0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;154m [0m[38;5;148m_[0m[38;5;184m/[0m[38;5;184m [0m[38;5;184m [0m[38;5;184m_[0m[38;5;184m/[0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;178m [0m[38;5;214m [0m[38;5;214m_[0m[38;5;214m/[0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m_[0m[38;5;214m/[0m[38;5;208m [0m[38;5;208m [0m[38;5;208m_[0m[38;5;208m/[0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m_[0m[38;5;209m/[0m[38;5;203m [0m[38;5;203m [0m[38;5;203m_[0m[38;5;203m/[0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m_[0m[38;5;203m/[0m[38;5;203m [0m[38;5;204m [0m[38;5;198m_[0m[38;5;198m/[0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m_[0m[38;5;198m/[0m[38;5;198m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m_[0m[38;5;199m/[0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;163m [0m[38;5;163m [0m[38;5;164m [0m[38;5;164m [0m",
        "[38;5;154m [0m[38;5;154m_[0m[38;5;154m/[0m[38;5;148m_[0m[38;5;184m/[0m[38;5;184m_[0m[38;5;184m/[0m[38;5;184m [0m[38;5;184m [0m[38;5;184m_[0m[38;5;184m/[0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;178m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m_[0m[38;5;214m/[0m[38;5;214m_[0m[38;5;214m/[0m[38;5;208m_[0m[38;5;208m/[0m[38;5;208m [0m[38;5;208m [0m[38;5;208m_[0m[38;5;208m/[0m[38;5;208m_[0m[38;5;208m/[0m[38;5;208m_[0m[38;5;209m/[0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m_[0m[38;5;203m/[0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m_[0m[38;5;204m/[0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m_[0m[38;5;198m/[0m[38;5;198m_[0m[38;5;198m/[0m[38;5;198m [0m[38;5;199m [0m[38;5;199m_[0m[38;5;199m/[0m[38;5;199m [0m[38;5;199m [0m[38;5;199m_[0m[38;5;199m/[0m[38;5;199m_[0m[38;5;199m/[0m[38;5;163m_[0m[38;5;163m/[0m[38;5;164m_[0m[38;5;164m/[0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m",
        "[38;5;148m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;178m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m_[0m[38;5;208m/[0m[38;5;208m [0m[38;5;209m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;204m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;163m [0m[38;5;163m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m",
        "[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;184m [0m[38;5;178m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;214m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m [0m[38;5;208m_[0m[38;5;209m/[0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;203m [0m[38;5;204m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;198m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;199m [0m[38;5;163m [0m[38;5;163m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;164m [0m[38;5;128m [0m",
        "${AnsiColor.DEFAULT}"};

    private static final String SPRING_BOOT = " :: Spring Boot         :: ";
    private static final String APPLICATION = " :: Application         :: ";
    private static final String GITINFO     = " :: Git branch / commit :: ";
    private static final String BUILDINFO   = " :: Build               :: ";
    private static final String BUILDTIME   = " :: created             :: ";

    private static final Logger LOGGER = LoggerFactory.getLogger(MyBanner.class);

    @Getter
    private GitProperties gitProperties;

    @Getter
    private BuildProperties buildProperties;


    public MyBanner() {
        Resource gitResource = new ClassPathResource("git.properties");
        Resource buildResource = new ClassPathResource("META-INF/build-info.properties");

        try {
            gitProperties = new GitProperties(loadFrom(gitResource, "git"));
            buildProperties = new BuildProperties(loadFrom(buildResource, "build"));

        } catch (IOException e) {
            // not fatal - only results in no version information being printed
            LOGGER.warn("Could not get necessary properties to print the complete banner.", e);
        }
    }

    @Override
    public void printBanner(Environment environment, Class<?> sourceClass, PrintStream printStream) {
        PropertyResolver resolver = getAnsiResolver();
        for (String line : BANNER) {
            printStream.println(resolver.resolvePlaceholders(line));
        }
        printStream.println(AnsiOutput.toString(AnsiColor.GREEN, SPRING_BOOT,
            AnsiColor.DEFAULT, AnsiStyle.FAINT, SpringBootVersion.getVersion()));
        printStream.println(AnsiOutput.toString(AnsiColor.GREEN, APPLICATION,
            AnsiColor.DEFAULT, AnsiStyle.FAINT, getApplicationTitle(sourceClass)));
        if (gitProperties != null) {
            printStream.println(AnsiOutput.toString(AnsiColor.GREEN, GITINFO,
                AnsiColor.DEFAULT, AnsiStyle.FAINT, gitProperties.getBranch() + " / " + gitProperties.getCommitId()));
        } else {
            printStream.println(AnsiOutput.toString(AnsiColor.GREEN, GITINFO, AnsiColor.RED, "no git info available"));
        }
        printStream.println(AnsiOutput.toString(AnsiColor.GREEN, BUILDINFO,
            AnsiColor.DEFAULT, AnsiStyle.FAINT, buildProperties.getVersion()));
        final Instant buildTime = buildProperties.getTime();
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        dateFormat.setTimeZone(TimeZone.getTimeZone("CET"));
        String formatedBuildTime = "unknown";
        try {
            formatedBuildTime = dateFormat.format(Date.from(buildTime));
        } catch (Exception e) {
            LOGGER.debug("Buildtime was not set correctly in build script: {}", e.getMessage(), e);
        }
        printStream.println(AnsiOutput.toString(AnsiColor.GREEN, BUILDTIME,
            AnsiColor.DEFAULT, AnsiStyle.FAINT, formatedBuildTime));
        printStream.println(AnsiOutput.toString(AnsiStyle.NORMAL));
    }

    private PropertyResolver getAnsiResolver() {
        MutablePropertySources sources = new MutablePropertySources();
        sources.addFirst(new AnsiPropertySource("ansi", true));
        return new PropertySourcesPropertyResolver(sources);
    }

    private String getApplicationTitle(Class<?> sourceClass) {
        Package sourcePackage = (sourceClass == null ? null : sourceClass.getPackage());
        return (sourcePackage == null ? null : sourcePackage.getImplementationTitle());
    }

    /**
     * Helper method to strip prefix for InfoProperties
     * @param location
     * @param prefix
     * @return
     * @throws IOException
     */
    private Properties loadFrom(Resource location, String prefix) throws IOException {
        String p = prefix.endsWith(".") ? prefix : prefix + ".";
        Properties source = PropertiesLoaderUtils.loadProperties(location);
        Properties target = new Properties();
        for (String key : source.stringPropertyNames()) {
            if (key.startsWith(p)) {
                target.put(key.substring(p.length()), source.get(key));
            }
        }
        return target;
    }
}
