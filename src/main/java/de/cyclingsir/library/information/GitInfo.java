package de.cyclingsir.library.information;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * This provides information which git commit
 * the build artifact is based on
 * Created by M.R. on 02.02.2017.
 */
@Getter
@Component
@PropertySource("classpath:git.properties")
public class GitInfo {
    @Value("${git.branch}")
    private String branch;

    @Value("${git.commit.id}")
    private String commitId;

    @Value("${git.commit.time}")
    private String timestamp;
}
