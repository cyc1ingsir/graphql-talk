package de.cyclingsir.library.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import de.cyclingsir.library.entity.BookCategory;
import de.cyclingsir.library.entity.BookCategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Created by cyc1ingsir on 4/19/18.
 */
@Component
@RequiredArgsConstructor
public class LibraryCategoryQuery implements GraphQLQueryResolver {

    private final BookCategoryRepository bookCategoryRepository;

    public Iterable<BookCategory> categories() {
        return bookCategoryRepository.findAll();
    }
}
