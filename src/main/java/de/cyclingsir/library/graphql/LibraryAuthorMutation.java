package de.cyclingsir.library.graphql;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import de.cyclingsir.library.entity.Author;
import de.cyclingsir.library.entity.AuthorRepository;
import de.cyclingsir.library.entity.Book;
import de.cyclingsir.library.entity.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Created by cyc1ingsir on 4/17/18.
 */
@Component
@RequiredArgsConstructor
public class LibraryAuthorMutation implements GraphQLMutationResolver {

    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;

    public Author createAuthor(Author author) {
        final Author responseAuthor = authorRepository.save(author);

        for (Book book : responseAuthor.getBooks()) {
            if (book.getId() == 0) {
                book.setAuthor(responseAuthor);
                bookRepository.save(book);
            }
        }

        return  responseAuthor;
    }

}
