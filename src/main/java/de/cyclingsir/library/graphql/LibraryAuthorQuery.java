package de.cyclingsir.library.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import de.cyclingsir.library.entity.Author;
import de.cyclingsir.library.entity.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by cyc1ingsir on 4/13/18.
 */
@Component
@RequiredArgsConstructor
public class LibraryAuthorQuery implements GraphQLQueryResolver {

    private final AuthorRepository authorRepository;

    public Iterable<Author> authors() {
        return authorRepository.findAll();
    }

    public Optional<Author> author(Long id) {
        return authorRepository.findById(id);
    }

}
