package de.cyclingsir.library.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import de.cyclingsir.library.entity.Book;
import de.cyclingsir.library.entity.BookRepository;
import de.cyclingsir.library.entity.Reader;
import de.cyclingsir.library.entity.ReaderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by cyc1ingsir on 4/13/18.
 */
@Component
@RequiredArgsConstructor
public class LibraryBookQuery implements GraphQLQueryResolver {

    private final BookRepository bookRepository;
    private final ReaderRepository readerRepository;


    public Iterable<Book> books() {
        return bookRepository.findAll();
    }

    public Iterable<Book> pagedBooks(int count, int offset) {
        return bookRepository.findAll(PageRequest.of(offset, count));
    }

    public Optional<Reader> reader(Long id) {
        return readerRepository.findById(id);
    }

}
