package de.cyclingsir.library.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import de.cyclingsir.library.entity.Publisher;
import de.cyclingsir.library.entity.PublisherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * Created by cyc1ingsir on 4/19/18.
 */
@Component
@RequiredArgsConstructor
public class LibraryPublisherQuery implements GraphQLQueryResolver {

    private final PublisherRepository publisherRepository;

    public Iterable<Publisher> publisher() {
        return publisherRepository.findAll();
    }

}
