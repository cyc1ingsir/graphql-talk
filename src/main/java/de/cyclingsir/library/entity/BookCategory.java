package de.cyclingsir.library.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by cyc1ingsir on 12/04/18.
 */
@Entity
@Data
@Table(name = "category")
public class BookCategory {

    @Id
    private long id;

    private String main;

    private String sub;

    @OneToMany(mappedBy = "category")
    private List<Book> books;

}
