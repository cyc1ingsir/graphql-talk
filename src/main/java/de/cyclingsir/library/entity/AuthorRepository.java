package de.cyclingsir.library.entity;

/**
 * Created by cyc1ingsir on 12/04/18.
 */

import org.springframework.data.repository.PagingAndSortingRepository;

public interface AuthorRepository extends PagingAndSortingRepository<Author, Long> {
}
