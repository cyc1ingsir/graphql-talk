package de.cyclingsir.library.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by cyc1ingsir on 12/04/18.
 */
@Entity
@Data
public class Publisher {

    @Id
    private long id;

    private String name;

    private String town;

    @OneToMany(mappedBy = "publisher")
    private List<Book> books;
}
