package de.cyclingsir.library.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by cyc1ingsir on 4/19/18.
 */
@Repository
public interface BookCategoryRepository extends CrudRepository<BookCategory, Long> {
}
