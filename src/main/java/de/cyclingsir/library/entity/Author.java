package de.cyclingsir.library.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created by cyc1ingsir on 12/04/18.
 */
@Entity
@Data
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String firstname;

    private String surname;

    private int born;

    private Integer died;

    @OneToMany(mappedBy = "author")
//    @JsonManagedReference
    @JsonDeserialize(using = BookListDeserializer.class)
    private List<Book> books;

}
