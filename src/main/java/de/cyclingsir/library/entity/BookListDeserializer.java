package de.cyclingsir.library.entity;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by cyc1ingsir on 4/18/18.
 */
@Component
public class BookListDeserializer extends JsonDeserializer<List<Book>> {

    private static PublisherRepository publisherRepository;

    private static BookCategoryRepository bookCategoryRepository;

    /*
     * This looks like a workaround :(
     * https://stackoverflow.com/questions/17576098/how-to-autowire-some-bean-into-jsonserializer#21562939
     */
    @Autowired
    public BookListDeserializer(PublisherRepository publisherRepository, BookCategoryRepository bookCategoryRepository) {
       this.publisherRepository = publisherRepository;
       this.bookCategoryRepository = bookCategoryRepository;
    }

    public BookListDeserializer() {
    }

    @Override
    public List<Book> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {

        JsonNode node = jp.getCodec().readTree(jp);

        final ArrayList<Book> books = new ArrayList<>();

        for (Iterator<JsonNode> it = node.elements(); it.hasNext();) {
            JsonNode child = it.next();
            books.add(constructBook(child));
        }

        return books;
    }

    private Book constructBook(JsonNode node) throws IOException {
        final Book book = new Book();

        long categoryId = node.get("categoryID").asLong();
        book.setCategory(bookCategoryRepository.findById(categoryId).get());

        long publisherId = node.get("publisherID").asLong();
        book.setPublisher(publisherRepository.findById(publisherId).get());

        book.setTitle(node.get("title").asText());
        book.setBlurb(node.get("blurb").asText());
        book.setPages(node.get("pages").asInt());

        return book;
    }
}
