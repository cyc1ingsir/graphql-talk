package de.cyclingsir.library.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;

/**
 * Created by cyc1ingsir on 12/04/18.
 */
@Entity
@Data
public class Book {

    @Id
    private long id;

    @Basic
    private String title;

    @Basic
    private int pages;

    @Basic
    private String blurb;

    @ManyToOne
    @JoinColumn(name = "authorid")
    private Author author;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "publisherid")
    private Publisher publisher;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryid")
    private BookCategory category;

    @ManyToMany(mappedBy = "books")
    @JsonBackReference
    private List<Reader> readers;

}
