package de.cyclingsir.library.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;

/**
 * Created by cyc1ingsir on 12/04/18.
 */
@Entity
@Data
public class Reader {

    @Id
    private long id;

    private String firstname;

    private String surname;

    private int born;

    private String address;

    @ManyToMany
    @JoinTable(name = "book_reader", joinColumns = @JoinColumn(name = "bookid", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "readerid", referencedColumnName = "id"))
//    @JsonManagedReference
    private List<Book> books;

}
