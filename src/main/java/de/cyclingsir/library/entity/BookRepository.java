package de.cyclingsir.library.entity;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

/**
 * Created by cyc1ingsir on 12/04/18.
 */
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {

    List<Book> findByAuthorId(long authorId);
}
