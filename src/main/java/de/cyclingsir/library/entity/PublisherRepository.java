package de.cyclingsir.library.entity;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by cyc1ingsir on 4/14/18.
 */
@Repository
public interface PublisherRepository extends CrudRepository<Publisher, Long> {
}
