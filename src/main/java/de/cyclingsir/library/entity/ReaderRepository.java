package de.cyclingsir.library.entity;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by cyc1ingsir on 12/04/18.
 */
public interface ReaderRepository extends CrudRepository<Reader, Long> {
}
