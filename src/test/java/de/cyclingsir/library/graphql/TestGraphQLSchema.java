package de.cyclingsir.library.graphql;

import com.coxautodev.graphql.tools.SchemaObjects;
import com.coxautodev.graphql.tools.SchemaParser;
import de.cyclingsir.library.entity.AuthorRepository;
import de.cyclingsir.library.entity.BookCategoryRepository;
import de.cyclingsir.library.entity.BookRepository;
import de.cyclingsir.library.entity.PublisherRepository;
import de.cyclingsir.library.entity.ReaderRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;

/**
 * Created by cyc1ingsir on 4/14/18.
 */
public class TestGraphQLSchema {

    private SchemaParser schemaParser;

    @Mock
    private AuthorRepository authorRepository;
    @Mock
    private ReaderRepository readerRepository;
    @Mock
    private BookRepository bookRepository;
    @Mock
    private BookCategoryRepository bookCategoryRepository;
    @Mock
    private PublisherRepository publisherRepository;

    @Before
    public void setup() throws IOException, URISyntaxException {

        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        Resource[] resources = resolver.getResources("graphql/*.graphqls");

        String[] files = Arrays.stream(resources).map(resource -> {
            try {
                final File file = resource.getFile();
                return new String(file.getParentFile().getName() + "/" + file.getName());
            } catch (IOException e) {
                //skipping
            }
            return new String();
        }).filter(fileName -> !fileName.isEmpty()).toArray(String[]::new);

        schemaParser = schemaParser.newParser()
            .files(files)
            .resolvers(new LibraryBookQuery(bookRepository, readerRepository)
                , new LibraryAuthorQuery(authorRepository)
                , new LibraryAuthorMutation(authorRepository, bookRepository)
                , new LibraryCategoryQuery(bookCategoryRepository)
                , new LibraryPublisherQuery(publisherRepository))
            .build();
    }

    @Test
    public void validateSchema() {
        SchemaObjects objects = schemaParser.parseSchemaObjects();
    }
}
