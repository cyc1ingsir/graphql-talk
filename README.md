# Showcase

### Dokumentation
weitere Dokumentation findet sich im Ordner
[./doc/](./doc/)

### Start
Es handelt sich um eine Springboot Anwendung welche mit dem Gradle Wrapper gebaut wird.
Der Start der Anwendung erfolgt via
`./gradlew bootRun -Dspring.profiles.active=<Profile>`.
Damit der  Befehl funktioniert, wurde die [`build.gradle`](`build.gradle`) um folgenden Block erweitert:
```groovy
bootRun {
    systemProperties System.properties
}
```

